<?php

/**
 * Class Manager
 *
 * Permet de gérer les tâches (ajout/suppression et liste)
 */
class Manager
{
    /**
     * @var Sortie Permet d'afficher des textes dans le terminal
     */
    private $sortie;

    /**
     * @var array Liste des tâches du gestionnaire
     */
    private $content = [];

    /**
     * @var bool Le script est-il en cours d'execution ?
     */
    private $enCoursDExecution = true;

    /**
     * Méthode qui permet d'ajouter une tâche au gestionnaire
     *
     * @return void
     */
    private function ajouterTache(): void
    {
        do {
            echo "Quel est le nom de votre tâche ? (min 3 characters)\n > ";
            $nomDeLaTache = trim(fgets(STDIN));
        } while (strlen($nomDeLaTache) < 3);
        $this->content[] = new Task($nomDeLaTache);
    }

    /**
     * Méthode qui permet d'afficher l'aide
     */
    public function afficherAide()
    {
        echo "liste des commandes possibles : 
                aide|a          afficher les commandes possibles
                tache|t         créér une nouvelle tâche
                liste|l         afficher la liste des tâches en cours
                supprimer|s     supprimer une tâche existante
                quitter|q       arrêter l'éxécution du script\n";
    }

    /**
     * Méthode qui permet d'afficher la liste des tâches
     * ou un message si il n'y en a pas
     */
    public function __toString()
    {
        $retour = "";

        if (count($this->content) > 0) {
            foreach ($this->content as $tache) {
                $retour .= $tache . "\n";
            }
        } else {
            $retour .= "La liste est vide\n";
        }

        return $retour;
    }

    /**
     * Méthode qui permet de supprimer une tâche
     */
    private function supprimerTache()
    {
        // on affiche la liste des tâches
        echo $this;

        // on demande à l'utilisateur quelle tâche supprimer et on vérifie
        // si elle existe (on peut aussi sortir de la boucle sans rien saisir)
        do {
            echo "Quelle tache supprimer ?\n";
            $idASupprimer = intval(fgets(STDIN));
        } while (!$this->idExiste($idASupprimer) && $idASupprimer !== 0);

        // on supprimer la tâche du tableau
        $nouveauTableau = [];
        foreach ($this->content as $tache) {
            if ($idASupprimer != $tache->getId()) {
                $nouveauTableau[] = $tache;
            }
        }
        $this->content = $nouveauTableau;
    }

    /**
     * Permet de tester si un identifiant est utilisé par une des tâches
     * présente dans le gestionnaire
     *
     * @param int $id Identifiant à vérifier
     * @return bool Retrourne true si l'identifiant existe, false si il n'existe pas
     */
    private function idExiste(int $id): bool
    {
        foreach ($this->content as $tache) {
            if ($tache->getId() === $id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Méthode qui permet d'orienter l'execution du scription
     * en fonction du choix de l'utilisateur
     */
    public function choice()
    {
        $this->sortie->afficherTexte("Que souhaitez vous faire ?", "violet");
        $this->sortie->afficherTexte(" (aide, liste, tache, supprimer, quitter)", "marron");
        echo "\n > ";
        $action = trim(fgets(STDIN));

        switch ($action) {
            case 'aide':
            case 'a':
                $this->afficherAide();
                break;
            case 'tache':
            case 't':
                $this->ajouterTache();
                break;
            case 'liste':
            case 'l':
                echo $this;
                break;
            case 'supprimer':
            case 's':
                $this->supprimerTache();
                break;
            case 'quitter':
            case 'q':
                $this->enCoursDExecution = false;
                break;
        }
    }

    /**
     * Manager constructor.
     *
     * Permet de faire le choix en boucle tant que le script est en cours d'execution
     */
    public function __construct()
    {
        $this->sortie = new Sortie();

        do {
            $this->choice();
        } while ($this->enCoursDExecution);
    }

}
