<?php

class Sortie
{
    public function afficherTexte($texte, $couleur)
    {
        switch ($couleur) {
            case 'vert':
                echo "\e[0;32m";
                break;
            case 'bleu':
                echo "\e[0;34m";
                break;
            case 'turquoise':
                echo "\e[0;36m";
                break;
            case 'rouge':
                echo "\e[0;31m";
                break;
            case 'violet':
                echo "\e[0;35m";
                break;
            case 'jaune':
                echo "\e[1;33m";
                break;
            case 'marron':
                echo "\e[0;33m";
                break;
        }

        echo $texte;

        echo "\e[0m";
    }
}
