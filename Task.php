<?php

/**
 * Class Task
 */
class Task
{

    /**
     * Attribut statique qui garde la même valeur dans
     * toutes les instances de la classe
     */
    static $nextId = 1;

    /**
     * Nom de la tâche
     */
    private $name;

    private $creationDate;

    /**
     * Identifiant unique de la tâche
     *
     * @var int
     */
    private $id;

    /**
     * Méthode pour récupérer l'identifiant de la tâche
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Méthode pour récupérer le nom de la tâche
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Task constructor.
     * @param string $name Nom de la tâche
     */
    public function __construct($name)
    {
        $this->creationDate = new DateTime();
        $this->name = $name;
        $this->id = $this::$nextId;
        $this::$nextId++;
    }

    /**
     * Converti l'instance en chaine de caractères
     * @return string
     */
    public function __toString()
    {
        return $this->creationDate->format("d-m-Y H:i:s") . " " . $this->name . " (" . $this->id . ")";
    }

}
